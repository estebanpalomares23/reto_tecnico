package co.com.choucair.certificacion.estebanpreto.tasks;

import co.com.choucair.certificacion.estebanpreto.userinterface.PersonalInformationPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;

public class PersonalUserInformation implements Task {

    public static Performable enter() {
        return Tasks.instrumented(PersonalUserInformation.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Enter.theValue("Doky").into(PersonalInformationPage.FIRST_NAME),
                        Enter.theValue("Cardenas").into(PersonalInformationPage.LAST_NAME),
                        Enter.theValue("losdosydoky@gmail.com").into(PersonalInformationPage.EMAIL),
                        SelectFromOptions.byVisibleText("June").from(PersonalInformationPage.MONTH_BIRTH),
                        SelectFromOptions.byVisibleText("10").from(PersonalInformationPage.DAY_BIRTH),
                        SelectFromOptions.byVisibleText("1992").from(PersonalInformationPage.YEAR_BIRTH),
                        Click.on(PersonalInformationPage.NEXT_BUTTON)
                );
    }
}
