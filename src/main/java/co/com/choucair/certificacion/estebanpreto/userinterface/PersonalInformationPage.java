package co.com.choucair.certificacion.estebanpreto.userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class PersonalInformationPage extends PageObject {
    public static final Target FIRST_NAME = Target.the("Write the first name").
            located(By.id("firstName"));
    public static final Target LAST_NAME = Target.the("Write the last name").
            located(By.id("lastName"));
    public static final Target EMAIL = Target.the("Write the email").
            located(By.id("email"));
    public static final Target MONTH_BIRTH =Target.the("Select month of birth").
            located(By.id("birthMonth"));
    public static final Target DAY_BIRTH = Target.the("Select day of birth").
            located(By.id("birthDay"));
    public static final Target YEAR_BIRTH =Target.the("Select year of birth").
            located(By.xpath("/html/body/ui-view/main/section/div/div[2]/div/div[2]/div/form/div[1]/div[3]/div[4]/div[2]/div/div[3]/select"));
    public static final Target NEXT_BUTTON =Target.the("Click on netx button").
            located(By.xpath("/html/body/ui-view/main/section/div/div[2]/div/div[2]/div/form/div[2]/a/span"));
}
