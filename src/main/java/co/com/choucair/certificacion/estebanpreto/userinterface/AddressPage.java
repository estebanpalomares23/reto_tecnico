package co.com.choucair.certificacion.estebanpreto.userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class AddressPage extends PageObject {

    public static final Target CITY = Target.the("Write the user city location").
            located(By.id("city"));
    public static final Target ZIP_CODE = Target.the("Write the zip code").
            located(By.id("zip"));
    public static final Target COUNTRY = Target.the("Wirte the country").
            located(By.className("//*[@id=\"regs_container\"]/div/div[2]/div/div[2]/div/form/div[1]/div[3]/div[1]/div[4]/div[2]/div/div/div[1]/span/span[2]"));
    public static final Target NEXT_BUTTON = Target.the("Next Button").
            located(By.xpath("/html/body/ui-view/main/section/div/div[2]/div/div[2]/div/form/div[2]/div/a"));

}
