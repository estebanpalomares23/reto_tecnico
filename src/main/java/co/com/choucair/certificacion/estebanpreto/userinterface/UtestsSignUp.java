package co.com.choucair.certificacion.estebanpreto.userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class UtestsSignUp extends PageObject {
    public static final Target JOIN_BUTTON = Target.the("Button to start SignUp")
            .locatedBy("/html/body/ui-view/unauthenticated-container/div/div/unauthenticated-header/div/div[3]/ul[2]/li[2]/a");
}
