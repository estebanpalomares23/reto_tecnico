package co.com.choucair.certificacion.estebanpreto.tasks;

import co.com.choucair.certificacion.estebanpreto.userinterface.AddressPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class AddressUser implements Task {

    public static AddressUser enter() {
        return Tasks.instrumented(AddressUser.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                        Enter.theValue("111071").into(AddressPage.ZIP_CODE),
                        Click.on(AddressPage.NEXT_BUTTON)

        );
    }
}
