package co.com.choucair.certificacion.estebanpreto.tasks;

import co.com.choucair.certificacion.estebanpreto.userinterface.DevicesInformationPage;
import com.gargoylesoftware.css.parser.selector.SelectorList;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;
import net.serenitybdd.screenplay.actions.selectactions.SelectByValueFromElement;

public class DevicesInformation implements Task {

    public static DevicesInformation enter() {
        return Tasks.instrumented(DevicesInformation.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(DevicesInformationPage.MOBILE_DEVICE)


        );
    }
}
