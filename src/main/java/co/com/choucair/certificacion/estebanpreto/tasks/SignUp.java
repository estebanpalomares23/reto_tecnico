package co.com.choucair.certificacion.estebanpreto.tasks;

import co.com.choucair.certificacion.estebanpreto.userinterface.UtestsSignUp;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

public class SignUp implements Task {


    public static SignUp start() {
        return Tasks.instrumented(SignUp.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(UtestsSignUp.JOIN_BUTTON));

    }
}
