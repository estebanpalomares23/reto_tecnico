package co.com.choucair.certification.estebanpreto.stepdefinitions;


import co.com.choucair.certificacion.estebanpreto.tasks.*;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;

public class ChoucairAcademyStepDefinitions {

    @Before
    public void setUp(){
        OnStage.setTheStage(new OnlineCast());
    }

    @Given("^than doky wants to create a new user in the utest website$")
    public void thanDokyWantsToCreateANewUserInTheUtestWebsite() {
        OnStage.theActorCalled("Doky").wasAbleTo(OpenUp.thepage());
    }


    @When("^he enter his personal information into the form$")
    public void heEnterHisPersoonalInformationIntoTheForm() {
        OnStage.theActorInTheSpotlight().attemptsTo(SignUp.start(),(PersonalUserInformation.enter()),(AddressUser.enter()),
                (DevicesInformation.enter()));
        
    }

    @Then("^he create a new user$")
    public void heCreateANewUser() {
    }
}
